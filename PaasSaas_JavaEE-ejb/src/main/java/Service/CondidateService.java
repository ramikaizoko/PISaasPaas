package Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import Entities.Condidate;

public class CondidateService {

	public List<Condidate> getall() throws JsonParseException, JsonMappingException, IOException {
		
		Client cl = ClientBuilder.newClient();
		WebTarget target = cl.target("http://localhost:23695/api"); 
		WebTarget hello = target.path("Condidate"); 
		 Response response1 = (Response) hello.request().get(); 
		 String resp = response1.readEntity(String.class); 
		 ObjectMapper mapper = new ObjectMapper();
		
	  
	     ArrayList<Condidate> mstCodes = mapper.readValue(resp, mapper.getTypeFactory().constructCollectionType(ArrayList.class, Condidate.class));
	         //System.out.println(mstCodes.size());
	      
	        return mstCodes;
	}
}
