package Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.json.JsonObject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import Entities.Condidate;
import Entities.HumanRessources;
import Entities.JWToken;
import Entities.ReferingEmployees;
import Entities.User;

@Stateless
@LocalBean
@JsonDeserialize

public class UserS {
	Client client;
	WebTarget target;
	String toke ="";
	String token="Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6InJhbWkiLCJyb2xlIjoiQWRtaW4iLCJDb21wYW55SWQiOiI0IiwiQ0lOIjoiNCIsIkhSU05hbWUiOiJmZiIsIkhSU1RleHQiOiJramgiLCJIUlNJbWFnZSI6ImpqIiwibmJmIjoxNTI0MzE2ODU0LCJleHAiOjE1MjY5MDg4NTQsImlhdCI6MTUyNDMxNjg1NH0.eMTTwUxIVyn40ce71Sep-TSQgw8w_qhAx4Rpmt1YiwM";
  //String token ="Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6IkFiaWRpIiwicm9sZSI6IkFkbWluIiwiQ29tcGFueUlkIjoiMSIsIkNJTiI6IjIiLCJIUlNOYW1lIjoia2hlZG1hIiwiSFJTVGV4dCI6IndlbGNvbWUiLCJIUlNJbWFnZSI6ImtrayIsIm5iZiI6MTUyNTIxNDc3NiwiZXhwIjoxNTI3ODA2Nzc2LCJpYXQiOjE1MjUyMTQ3NzZ9.goeue2hUpC1zxLwn5Vdv6cKFBQqt87xfkDx5TlHpSyY";
	public UserS() {
        
    	client = ClientBuilder.newClient();
		target = client.target("http://localhost:23695/api/"); 
		//target = client.target("http://paassaaswebapi-test.eu-west-1.elasticbeanstalk.com/"); 
    	//target = client.target("http://jsonplaceholder.typicode.com/"); 
		
		
		
    	
    }
public String logIn(Condidate user) {
    	
    WebTarget	custem_target = target.path("Authh");
    	JWToken tok =new JWToken();
    	tok.setUsername("abidi");
    	tok.setPassword("rami"); 
    	String response = custem_target.request(MediaType.APPLICATION_JSON).post(Entity.entity(tok, MediaType.APPLICATION_JSON)).readEntity(String.class);
		System.out.println(response);
		toke="Bearer "+response;
    	return toke;
}
    
    
    public Condidate getById(int id) {
    	
    	WebTarget hello = target.path("Userr/"+id);
    	Response res=(Response) hello.request(MediaType.APPLICATION_JSON).header("Authorization", token).get();
    	System.out.println(res.getStatusInfo().getStatusCode());
    	//System.out.println(res.readEntity(String.class));
    	Condidate cond = res.readEntity(Condidate.class);
		 System.out.println("ref"+cond);
		 System.out.println(cond.getLastName());
		 
		return cond;
		

    }
    public String AddUser(Condidate u) {
    	WebTarget adu = target.path("Userr/");
    	//u.setState("inactive");
    	//u.setStatus("Refused");
    	u.setStatus(null);
    	
    	u.setRole("Admin");
    	User return_user = adu.request(MediaType.APPLICATION_JSON).header("Authorization", token).post(Entity.entity(u, MediaType.APPLICATION_JSON), User.class);
		System.out.println("returned user"+return_user.getCin());
		return return_user.getFirstName();
		
    }
    
    
    
    
    public String AddUserr(ReferingEmployees u) {
    	WebTarget nv = target.path("Userref/");
    	//u.setState("inactive");
    	//u.setStatus("Refused");
    
    	
    	u.setRole("Admin");
    	User return_user = nv.request(MediaType.APPLICATION_JSON).header("Authorization", token).post(Entity.entity(u, MediaType.APPLICATION_JSON), User.class);
		System.out.println("returned user"+return_user.getCin());
		return return_user.getFirstName();
		
    }
    
    
  public ReferingEmployees getByIdRef(int id) {
    	
    	WebTarget hello = target.path("Userref/"+id);
    	Response res=(Response) hello.request(MediaType.APPLICATION_JSON).header("Authorization", token).get();
    	System.out.println(res.getStatusInfo().getStatusCode());
    	//System.out.println(res.readEntity(String.class));
    	ReferingEmployees post = res.readEntity(ReferingEmployees.class);
		 System.out.println("ref"+post);
		 System.out.println(post.getLastName());
		 
		return post;
		

    }
  List<Condidate> con = new ArrayList<Condidate>();
  
    
    public List<Condidate> getCon() {
	return con;
}


public void setCon(List<Condidate> con) {
	this.con = con;
}


	/*public List<Condidate> getAll () {
    	
  	   System.out.println("les users ");
  	 System.out.println("////////////////////");
  	   target = target.path("Userr");
  	  Response res=(Response) target.request(MediaType.APPLICATION_JSON).header("Authorization", token).get();
List<Condidate> post = res.readEntity(new GenericType<List <Condidate>>() {}) ;
 
  // Collection<Condidate> readValues = new ObjectMapper().readValue(jsonAsString, new TypeReference<Collection<Condidate>>() { });	
   // Condidate[]  post = res.readEntity(Condidate[].class);
  	/* for(Condidate c:post){
  		con.add(c);
  	 }
   System.out.println(post);
  	 return post;
    }*/


    
    
    public ArrayList<ReferingEmployees> getAllR () throws JsonParseException, JsonMappingException, IOException {
    	WebTarget cnd = target.path("Userref/");
    	// Response response1 = (Response) hello.request().get(); 
    	 Response res=(Response) cnd.request(MediaType.APPLICATION_JSON).header("Authorization", token).get();
    	 String resp = res.readEntity(String.class); 
    	//System.out.println(resp);
    	//System.out.println(response1.getStatus());
    	 ObjectMapper mapper = new ObjectMapper();
    	
         
    	 ArrayList<ReferingEmployees> mstCodes = mapper.readValue(resp, mapper.getTypeFactory().constructCollectionType(ArrayList.class, ReferingEmployees.class));
             //System.out.println(mstCodes.size());
           System.out.println(mstCodes);
            return mstCodes;
    }
    
    
    
    
    
    
    public Response update( Condidate u, int id) 
	{
		
		WebTarget upc = target.path("userr/"+id);
		Response res=(Response) upc.request(MediaType.APPLICATION_JSON).header("Authorization", token).put(Entity.json(u));
    	System.out.println("status code = "+res.getStatus());
    	System.out.println(u.toString());
    	
    	return res;
	}
    
    
	
	
    
    
    
    public Response delete(int id) 
	{
		
    	WebTarget dlc = target.path("userr/"+id);
		Response res=(Response) dlc.request(MediaType.APPLICATION_JSON).header("Authorization", token).delete();
		
		System.out.println(res.getStatusInfo().getStatusCode());
	//System.out.println(res.readEntity(String.class));
	 
	 
	return res;
	}

    
    
    
    
    
    public List<Condidate> getAll() throws JsonParseException, JsonMappingException, IOException  {
    //	Client cl = ClientBuilder.newClient();
    
    	WebTarget cnd = target.path("Userr/");
    	// Response response1 = (Response) hello.request().get(); 
    	 Response res=(Response) cnd.request(MediaType.APPLICATION_JSON).header("Authorization", token).get();
    	 String resp = res.readEntity(String.class); 
    	//System.out.println(resp);
    	//System.out.println(response1.getStatus());
    	 ObjectMapper mapper = new ObjectMapper();
    	
         
             List<Condidate> mstCodes = mapper.readValue(resp, mapper.getTypeFactory().constructCollectionType(List.class, Condidate.class));
             //System.out.println(mstCodes.size());
           System.out.println(mstCodes);
            return mstCodes;
       
    	
     }
    public List<HumanRessources> getAllhu() throws JsonParseException, JsonMappingException, IOException  {
        //	Client cl = ClientBuilder.newClient();
        
        	WebTarget cnd = target.path("HumanRessourcess/");
        	// Response response1 = (Response) hello.request().get(); 
        	 Response res=(Response) cnd.request(MediaType.APPLICATION_JSON).header("Authorization", token).get();
        	 String resp = res.readEntity(String.class); 
        	//System.out.println(resp);
        	//System.out.println(response1.getStatus());
        	 ObjectMapper mapper = new ObjectMapper();
        	
             
                 List<HumanRessources> mstCodes = mapper.readValue(resp, mapper.getTypeFactory().constructCollectionType(List.class, HumanRessources.class));
                 //System.out.println(mstCodes.size());
               System.out.println(mstCodes);
                return mstCodes;
           
        	
         }
    
    
    
    
    
    public HumanRessources getAllh() throws JsonParseException, JsonMappingException, IOException  {
    //	Client cl = ClientBuilder.newClient();
    
    	WebTarget hst = target.path("HumanRessourcess/");
    	// Response response1 = (Response) hello.request().get(); 
    	 Response res=(Response) hst.request(MediaType.APPLICATION_JSON).header("Authorization", token).get();
    	 HumanRessources resp = res.readEntity(HumanRessources.class); 
    	//System.out.println(resp);
    	System.out.println(resp.getStatus());
    	 /*ObjectMapper mapper = new ObjectMapper();
    	
         
    	 List<HumanRessources> mstCodes = mapper.readValue(resp, mapper.getTypeFactory().constructCollectionType(List.class, HumanRessources.class));
             //System.out.println(mstCodes.size());*/
           System.out.println(resp);
            return resp;
       
    	
     }
    
    
    
    
    public Response updateR( ReferingEmployees u, int id) 
   	{
   		
   		WebTarget upc = target.path("userref/"+id);
   		Response res=(Response) upc.request(MediaType.APPLICATION_JSON).header("Authorization", token).put(Entity.json(u));
       	System.out.println("status code = "+res.getStatus());
       	System.out.println(u.toString());
       	
       	return res;
   	}
       
       public Response deleteR(int id) 
   	{
   		
       	WebTarget dlc = target.path("userref/"+id);
   		Response res=(Response) dlc.request(MediaType.APPLICATION_JSON).header("Authorization", token).delete();
   		
   		System.out.println(res.getStatusInfo().getStatusCode());
   	//System.out.println(res.readEntity(String.class));
   	 
   	 
   	return res;
   	}
    	 
    }
    
    
    
    
    

