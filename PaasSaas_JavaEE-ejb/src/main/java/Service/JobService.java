package Service;

import java.util.List;
import java.io.IOException;
import java.util.ArrayList;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import Entities.Job;

public class JobService {

	public List<Job> getJobNoneSaturated() throws JsonParseException, JsonMappingException, IOException{
		
		//List<Job> jobNoneSaturated =  new ArrayList<>();
		Client c = ClientBuilder.newClient();
		WebTarget target = c.target("http://localhost:23695/api/job/");
		Response rsl = (Response) target.request().get(); 
		String resp = rsl.readEntity(String.class); 
		ObjectMapper mapper = new ObjectMapper();
		ArrayList<Job> jobNoneSaturated = mapper.readValue
				(resp, mapper.getTypeFactory().constructCollectionType(ArrayList.class, Job.class));
		
		return jobNoneSaturated;
	}
}
