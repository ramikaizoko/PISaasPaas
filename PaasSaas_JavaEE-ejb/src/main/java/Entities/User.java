package Entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
@JsonIgnoreProperties(ignoreUnknown = true)
public class User {
	@JsonIgnore
	public int Status;
	@JsonIgnore
	public int Message;
	@JsonIgnore
	public int $ref;
	@JsonIgnore
	public int $id ;
	@JsonIgnore
	public int id ;
	@JsonProperty("CIN")
	public int cin ;
	@JsonProperty("FirstName")
	public String firstName ;
	@JsonProperty("LastName")
	public String lastName ;
	@JsonProperty("Email")
	public String email ;
	@JsonProperty("Password")
	public String password ;
	@JsonProperty("Role")
	public String role ;
	@JsonProperty("Score")
	public float score;
	public float getScore() {
		return score;
	}
	public void setScore(float score) {
		this.score = score;
	}

	@JsonProperty("CompanyId")
	public int companyId;
	@JsonProperty("HRs")
	public HumanRessources hrs;
	
	
	public HumanRessources getHrs() {
		return hrs;
	}
	public void setHrs(HumanRessources hrs) {
		this.hrs = hrs;
	}
	public int getCin() {
		return cin;
	}
	public void setCin(int cin) {
		this.cin = cin;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}

	@Override
	public String toString() {
		return "User [Status=" + Status + ", Message=" + Message + ", $ref=" + $ref + ", $id=" + $id + ", cin=" + cin
				+ ", firstName=" + firstName + ", lastName=" + lastName + ", email=" + email + ", password=" + password
				+ ", role=" + role + /*", score=" + score + */", companyId=" + companyId + ", hrs=" + hrs + "]";
	}
	
	
}
