package Entities;

import java.util.ArrayList;

import org.codehaus.jackson.annotate.JsonProperty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@JsonIgnoreProperties(ignoreUnknown = true)
public class ReferingEmployees extends User {
	@JsonProperty("Role")
	public String role;
	@JsonProperty("Score")
	public float score;
	
	private Job job;
	private ArrayList<Notification> myNotifs;
	private ArrayList<Condidate> condidate;
	
	
	public Job getJob() {
		return job;
	}
	public void setJob(Job job) {
		this.job = job;
	}
	public ArrayList<Condidate> getCondidate() {
		return condidate;
	}
	public void setCondidate(ArrayList<Condidate> condidate) {
		this.condidate = condidate;
	}
	public ArrayList<Notification> getMyNotifs() {
		return myNotifs;
	}
	public void setMyNotifs(ArrayList<Notification> myNotifs) {
		this.myNotifs = myNotifs;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public float getScore() {
		return score;
	}
	public void setScore(float score) {
		this.score = score;
	}
	

}
