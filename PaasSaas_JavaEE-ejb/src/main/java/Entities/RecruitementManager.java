package Entities;

import java.util.ArrayList;

import org.codehaus.jackson.annotate.JsonProperty;

public class RecruitementManager extends User {
	@JsonProperty("Role")
	public String role;
	private ArrayList<Condidate> condidate;
	private ArrayList<NewPost> newPosts;
	private ArrayList<Notification> myNotifis;
	
	
	public ArrayList<Condidate> getCondidate() {
		return condidate;
	}

	public void setCondidate(ArrayList<Condidate> condidate) {
		this.condidate = condidate;
	}

	public ArrayList<NewPost> getNewPosts() {
		return newPosts;
	}

	public void setNewPosts(ArrayList<NewPost> newPosts) {
		this.newPosts = newPosts;
	}

	public ArrayList<Notification> getMyNotifis() {
		return myNotifis;
	}

	public void setMyNotifis(ArrayList<Notification> myNotifis) {
		this.myNotifis = myNotifis;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
	
	
}
