package Entities;

public class Notification {

	private int notificationId;
	private String note;
	
	private RecruitementManager rm;
	private ReferingEmployees re;
	
	public RecruitementManager getRm() {
		return rm;
	}
	public void setRm(RecruitementManager rm) {
		this.rm = rm;
	}
	public ReferingEmployees getRe() {
		return re;
	}
	public void setRe(ReferingEmployees re) {
		this.re = re;
	}
	public int getNotificationId() {
		return notificationId;
	}
	public void setNotificationId(int notificationId) {
		this.notificationId = notificationId;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	
	
}
