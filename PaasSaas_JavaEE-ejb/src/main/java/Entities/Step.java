package Entities;

public class Step {

	private int Id;
	private String description;
	private float value;
	private State status;
	private Interview interviews;
	private HumanRessources hr;
	
	
	public Interview getInterviews() {
		return interviews;
	}

	public void setInterviews(Interview interviews) {
		this.interviews = interviews;
	}

	public HumanRessources getHr() {
		return hr;
	}

	public void setHr(HumanRessources hr) {
		this.hr = hr;
	}

	private  enum State { Accepted, Refused, progressing }

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public float getValue() {
		return value;
	}

	public void setValue(float value) {
		this.value = value;
	}

	public State getStatus() {
		return status;
	}

	public void setStatus(State status) {
		this.status = status;
	}
	
	
}
