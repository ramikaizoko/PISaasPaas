package Entities;

import java.util.ArrayList;

public class Job  {

	private int jobId;
	private String Name;
	private int numberOfPlaces;
	private int occupatedPlaces;
	
	private HumanRessources hrs;
	private ArrayList<Condidate> condidates;
	private ArrayList<ReferingEmployees> RefEmployees;
	
	
	
	public HumanRessources getHrs() {
		return hrs;
	}
	public void setHrs(HumanRessources hrs) {
		this.hrs = hrs;
	}
	public ArrayList<Condidate> getCondidates() {
		return condidates;
	}
	public void setCondidates(ArrayList<Condidate> condidates) {
		this.condidates = condidates;
	}
	public ArrayList<ReferingEmployees> getRefEmployees() {
		return RefEmployees;
	}
	public void setRefEmployees(ArrayList<ReferingEmployees> refEmployees) {
		RefEmployees = refEmployees;
	}
	public int getJobId() {
		return jobId;
	}
	public void setJobId(int jobId) {
		this.jobId = jobId;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public int getNumberOfPlaces() {
		return numberOfPlaces;
	}
	public void setNumberOfPlaces(int numberOfPlaces) {
		this.numberOfPlaces = numberOfPlaces;
	}
	public int getOccupatedPlaces() {
		return occupatedPlaces;
	}
	public void setOccupatedPlaces(int occupatedPlaces) {
		this.occupatedPlaces = occupatedPlaces;
	}
	
	
}
