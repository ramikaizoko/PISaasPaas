package Entities;

import java.sql.Date;
import java.util.ArrayList;

public class Interview {

	private int interviewId;
	private Date interviewDate;
	private float stepScore;
	
	private NewPost newPosts;
	private Condidate condidate;
	private ArrayList<Step> steps;
	
	
	
	public NewPost getNewPosts() {
		return newPosts;
	}
	public void setNewPosts(NewPost newPosts) {
		this.newPosts = newPosts;
	}
	public Condidate getCondidate() {
		return condidate;
	}
	public void setCondidate(Condidate condidate) {
		this.condidate = condidate;
	}
	public ArrayList<Step> getSteps() {
		return steps;
	}
	public void setSteps(ArrayList<Step> steps) {
		this.steps = steps;
	}
	public int getInterviewId() {
		return interviewId;
	}
	public void setInterviewId(int interviewId) {
		this.interviewId = interviewId;
	}
	public Date getInterviewDate() {
		return interviewDate;
	}
	public void setInterviewDate(Date interviewDate) {
		this.interviewDate = interviewDate;
	}
	public float getStepScore() {
		return stepScore;
	}
	public void setStepScore(float stepScore) {
		this.stepScore = stepScore;
	}
	
	
}
