package Entities;

import java.sql.Date;
import java.util.ArrayList;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;




@JsonIgnoreProperties(ignoreUnknown = true)
public class HumanRessources {
	@JsonIgnore
	public int Status;
	public int getStatus() {
		return Status;
	}
	public void setStatus(int status) {
		Status = status;
	}
	public int get$ref() {
		return $ref;
	}
	public void set$ref(int $ref) {
		this.$ref = $ref;
	}
	public int get$id() {
		return $id;
	}
	public void set$id(int $id) {
		this.$id = $id;
	}
	@JsonIgnore
	public int $ref;
	@JsonIgnore
	public int $id;
	@JsonProperty("CompanyId")
	private int companyId;
	@JsonProperty("NameCompany")
	private String nameCompany;
	@JsonProperty("MailCompany")
	private String mailCompany;
	@JsonProperty("AddressCompany")
	private String addressCompany;
	@JsonProperty("Image")
	private String image;
	@JsonProperty("WelcomeText")
	private String welcomeText;
	@JsonProperty("CreationDate")
	private Date creationDate;
	
	@JsonProperty("Users")

	private ArrayList<User> users;
	private ArrayList<Job> Jobs;
	private ArrayList<Step> Steps;
	public int getCompanyId() {
		return companyId;
	}
	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}
	public String getNameCompany() {
		return nameCompany;
	}
	public void setNameCompany(String nameCompany) {
		this.nameCompany = nameCompany;
	}
	public String getMailCompany() {
		return mailCompany;
	}
	public void setMailCompany(String mailCompany) {
		this.mailCompany = mailCompany;
	}
	public String getAddressCompany() {
		return addressCompany;
	}
	public void setAddressCompany(String addressCompany) {
		this.addressCompany = addressCompany;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getWelcomeText() {
		return welcomeText;
	}
	public void setWelcomeText(String welcomeText) {
		this.welcomeText = welcomeText;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public ArrayList<User> getUsers() {
		return users;
	}
	public void setUsers(ArrayList<User> users) {
		this.users = users;
	}
	public ArrayList<Job> getJobs() {
		return Jobs;
	}
	public void setJobs(ArrayList<Job> jobs) {
		Jobs = jobs;
	}
	public ArrayList<Step> getSteps() {
		return Steps;
	}
	public void setSteps(ArrayList<Step> steps) {
		Steps = steps;
	}
	
	

	
	
	
}
