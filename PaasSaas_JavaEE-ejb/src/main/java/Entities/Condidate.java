package Entities;

import org.codehaus.jackson.annotate.JsonProperty;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@JsonIgnoreProperties(ignoreUnknown = true)
public class Condidate extends User {

	@JsonProperty("Status")
	public States Status ;
	
	
	public  enum States { Accepted, Refused, progressing }
	
	
	//private RecruitementManager rm;
	//private ReferingEmployees re;
	
	
/*	public States getStatus() {
		return Status;
	}
	public void setStatus(States status) {
		status = Status;
	}*/
	

	@Override
	public String toString() {
		return "Condidate [Status=" + Status + ", Message=" + Message + ", $ref=" + $ref + ", $id=" + $id + ", cin="
				+ cin + ", firstName=" + firstName + ", lastName=" + lastName + ", email=" + email + ", password="
				+ password + ", role=" + role + ", score=" + score + ", companyId=" + companyId + ", hrs=" + hrs + "]";
	}


	public States getStatus() {
		return Status;
	}


	public void setStatus(States status) {
		Status = status;
	}
	

	
}
