package Entities;

import java.util.ArrayList;

public class NewPost {

	private int newPostId;
	private String name;
	private String description;
	
	private Job jb;
	private RecruitementManager rm;
	private ArrayList<Interview> interviews;
	
	
	public Job getJb() {
		return jb;
	}
	public void setJb(Job jb) {
		this.jb = jb;
	}
	public RecruitementManager getRm() {
		return rm;
	}
	public void setRm(RecruitementManager rm) {
		this.rm = rm;
	}
	public ArrayList<Interview> getInterviews() {
		return interviews;
	}
	public void setInterviews(ArrayList<Interview> interviews) {
		this.interviews = interviews;
	}
	public int getNewPostId() {
		return newPostId;
	}
	public void setNewPostId(int newPostId) {
		this.newPostId = newPostId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
