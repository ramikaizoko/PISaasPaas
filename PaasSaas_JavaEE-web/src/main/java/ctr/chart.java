
package ctr;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;

import java.io.IOException;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import org.primefaces.model.chart.PieChartModel;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import Entities.Condidate;
import Service.CondidateService;
import Service.UserS;
 
@ManagedBean
public class chart implements Serializable {
 
    private PieChartModel pieModel1;
    private PieChartModel pieModel2;
    
    
    int accepter ;
	int Refsuser ;
	int progression ;
	
	@EJB
	UserS userejb;
	
    @PostConstruct
    public void init() throws JsonParseException, JsonMappingException, IOException {
        createPieModels();
    }
 
    public PieChartModel getPieModel1() {
        return pieModel1;
    }
     
    public PieChartModel getPieModel2() {
        return pieModel2;
    }
     
    private void createPieModels() throws JsonParseException, JsonMappingException, IOException {
        createPieModel1();
        createPieModel2();
    }
 
    private void createPieModel1() {
        pieModel1 = new PieChartModel();
         
        pieModel1.set("Brand 1", 0);
        pieModel1.set("Brand 2", 200);
        pieModel1.set("Brand 3", 10);
         
        pieModel1.setTitle("Simple Pie");
        pieModel1.setLegendPosition("w");
    }
     
    private void createPieModel2() throws JsonParseException, JsonMappingException, IOException {
    	for (Condidate c : userejb.getAll()) 
    	{
			if (c.getStatus().toString().equals("Accepted"))
			{
				System.out.println("okkkkkk");
				accepter = accepter + 1;
				System.out.println("accepter="+accepter);
			}
			
			else if (c.getStatus().toString().equals("Refused"))
		    {
				System.out.println("okkkkkk2");
				Refsuser = Refsuser + 1;
				System.out.println("Refsuser="+Refsuser);
			} 
		    
			else 
		    {
				   System.out.println("okkkkkk3");
				   progression =  progression +1;
				   System.out.println("progression="+progression);
		    }
    	}
        pieModel2 = new PieChartModel();
         
        pieModel2.set("Candidates accepter ", accepter);
        pieModel2.set("Candidates refuser", Refsuser);
        pieModel2.set("Candidate en progressions", progression);
         
        pieModel2.setTitle("Candidate Statistique A");
        pieModel2.setLegendPosition("e");
        pieModel2.setFill(false);
        pieModel2.setShowDataLabels(true);
        pieModel2.setDiameter(150);
        
        //////////////////////
        pieModel1 = new PieChartModel();
        
        pieModel1.set("Candidates accepter ", accepter);
        pieModel1.set("Candidates refuser", Refsuser);
        pieModel1.set("Candidate en progressions", progression);
         
        pieModel2.setTitle("Candidate Statistique R");
        pieModel2.setLegendPosition("e");
        pieModel2.setFill(false);
        pieModel2.setShowDataLabels(true);
        pieModel2.setDiameter(150);
    }
     
}
