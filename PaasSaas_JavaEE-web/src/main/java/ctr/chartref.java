
package ctr;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;

import java.io.IOException;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import org.primefaces.model.chart.PieChartModel;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import Entities.ReferingEmployees;
import Service.UserS;
 
@ManagedBean
public class chartref implements Serializable {
 
    private PieChartModel pieModel1;
    private PieChartModel pieModel2;
    
    int bad;
    int good;
    int hero;
  
	
	@EJB
	UserS userejb;
	
    @PostConstruct
    public void init() throws JsonParseException, JsonMappingException, IOException {
        createPieModels();
    }
 
    public PieChartModel getPieModel1() {
        return pieModel1;
    }
     
    public PieChartModel getPieModel2() {
        return pieModel2;
    }
     
    private void createPieModels() throws JsonParseException, JsonMappingException, IOException {
        createPieModel1();
        createPieModel2();
    }
 
    private void createPieModel1() {
        pieModel1 = new PieChartModel();
         
        pieModel1.set("Brand 1", 0);
        pieModel1.set("Brand 2", 200);
        pieModel1.set("Brand 3", 10);
         
        pieModel1.setTitle("Simple Pie");
        pieModel1.setLegendPosition("w");
    }
     
    private void createPieModel2() throws JsonParseException, JsonMappingException, IOException {
    	for (ReferingEmployees c : userejb.getAllR()) 
    	{
			if (c.getScore()<10)
			{
				System.out.println("okkkkkk");
				bad = bad + 1;
				System.out.println("bad="+bad);
			}
			
			else if (c.getScore()>10 && c.getScore()<20)
		    {
				System.out.println("okkkkkk2");
				good = good + 1;
				System.out.println("good="+good);
			} 
		    
			else 
		    {
				   System.out.println("okkkkkk3");
				   hero =  hero +1;
				   System.out.println("hero="+hero);
		    }
    	}
        pieModel2 = new PieChartModel();
         
        pieModel2.set("bad ", bad);
        pieModel2.set("good", good);
        pieModel2.set("hero", hero);
         
        pieModel2.setTitle("Ref emp Statistique A");
        pieModel2.setLegendPosition("e");
        pieModel2.setFill(false);
        pieModel2.setShowDataLabels(true);
        pieModel2.setDiameter(150);
        
        //////////////////////
        pieModel1 = new PieChartModel();
        
        pieModel1.set("bad ", bad);
        pieModel1.set("good", good);
        pieModel1.set("hero", hero);
         
        pieModel1.setTitle("Ref emp Statistique R");
        pieModel1.setLegendPosition("e");
        pieModel1.setFill(false);
        pieModel1.setShowDataLabels(true);
        pieModel1.setDiameter(150);
    }
     
}
