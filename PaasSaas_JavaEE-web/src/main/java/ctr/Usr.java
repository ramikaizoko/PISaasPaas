package ctr;
import javax.faces.bean.ManagedBean;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.ejb.EJB;
import javax.faces.bean.SessionScoped;
import javax.ws.rs.core.Response;

import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.chart.LineChartSeries;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import Entities.Condidate;
import Entities.HumanRessources;
import Entities.ReferingEmployees;
import Entities.User;
import Entities.Condidate.States;
import Service.HumanRessourcesS;
import Service.UserS;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
@ManagedBean
@SessionScoped

public class Usr {
	
	static Properties mailServerProperties;
	static Session getMailSession;
	static MimeMessage generateMailMessage;
	Condidate cond;
	Condidate condd;
	Condidate condedit;
	List<Condidate> mstCode;
	List<ReferingEmployees> refemps;
	
	
	public List<ReferingEmployees> getRefemps() throws JsonParseException, JsonMappingException, IOException {
		return refemps=userejb.getAllR();
	}

	public void setRefemps(List<ReferingEmployees> refemps) {
		this.refemps = refemps;
	}

	public List<Condidate> getMstCode() throws JsonParseException, JsonMappingException, IOException {
		return mstCode =userejb.getAll();
		
	}

	public void setMstCode(List<Condidate> mstCode) {
		this.mstCode = mstCode;
	}
	String Txtcin;
	String Txtname;
	String Txtlname;
	String TxtStatus;
	String Txtscore;
	List<Condidate> liste2 ;
	List<ReferingEmployees> listeR ;
	
	
	public List<ReferingEmployees> getListeR() {
		return listeR;
	}

	public void setListeR(List<ReferingEmployees> listeR) {
		this.listeR = listeR;
	}
	int CndId;
	int Id;
    int cin;
	String statu;
	
	String emaill;
	String lastNam;
	String firstNam;
	String role;
	String password;
	float score;
	String login;
 public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}
States Status ;
	
	
	public  enum States { Accepted, Refused, progressing }
	

	public States getStatus() {
		return Status;
	}

	public void setStatus(States status) {
		Status = status;
	}

	public float getScore() {
		return score;
	}

	public void setScore(float score) {
		this.score = score;
	}

	public String getTxtlname() {
		return Txtlname;
	}

	public void setTxtlname(String txtlname) {
		Txtlname = txtlname;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getCin() {
		return cin;
	}

	public void setCin(int cin) {
		this.cin = cin;
	}
	
	public Condidate getCond() {
		return cond;
	}

	public Condidate getCondd() {
		return condd;
	}

	public Condidate getCondedit() {
		return condedit;
	}

	public String getStatu() {
		return statu;
	}

	public void setStatu(String statu) {
		this.statu = statu;
	}

	public String getEmaill() {
		return emaill;
	}

	public void setEmaill(String emaill) {
		this.emaill = emaill;
	}

	public String getLastNam() {
		return lastNam;
	}

	public void setLastNam(String lastNam) {
		this.lastNam = lastNam;
	}

	public String getFirstNam() {
		return firstNam;
	}

	public void setFirstNam(String firstNam) {
		this.firstNam = firstNam;
	}

	public List<Condidate> getliste2() {
		return liste2;
	}
	
	public String getTxtname() {
		return Txtname;
	}
	public void setTxtname(String txtname) {
		Txtname = txtname;
	}
	public String getTxtStatus() {
		return TxtStatus;
	}
	public void setTxtStatus(String txtStatus) {
		TxtStatus = txtStatus;
	}
	
	public String getTxtscore() {
		return Txtscore;
	}

	public void setTxtscore(String txtscore) {
		Txtscore = txtscore;
	}

	public String getTxtcin() {
		return Txtcin;
	}
	public void setTxtcin(String txtcin) {
		Txtcin = txtcin;
	}
	@EJB
	UserS userejb ;
	@EJB
	HumanRessourcesS humanejb ;	
User user = new User() ;
Condidate condidate = new Condidate() ;
ReferingEmployees refemp = new ReferingEmployees() ;
HumanRessources hmrs= new HumanRessources();


	public HumanRessources getHmrs() {
	return hmrs;
}

public void setHmrs(HumanRessources hmrs) {
	this.hmrs = hmrs;
}
public HumanRessources doGetAllh() throws JsonParseException, JsonMappingException, IOException {
	return userejb.getAllh();
}

	public ReferingEmployees getRefemp() {
	return refemp;
}

public void setRefemp(ReferingEmployees refemp) {
	this.refemp = refemp;
}

	public Condidate getCondidate() {
	return condidate;
}
public void setCondidate(Condidate condidate) {
	this.condidate = condidate;
}
	public User getUser() {
	return user;
}
public void setUser(User user) {
	this.user = user;
}
	public Condidate getById (){
		//return userejb.getById(4);
		return userejb.getById(2);
	}
	public ReferingEmployees getByIdRef(){
		return userejb.getByIdRef(5);
	}
	
	
	//////////////////////////////////////////////refering employees
	public String addr()throws AddressException, MessagingException
	{
		
		
		userejb.AddUserr(refemp);
		refemp= new ReferingEmployees();
		
		
		
		
		
		// Step1
				System.out.println("\n 1st ===> setup Mail Server Properties..");
				mailServerProperties = System.getProperties();
				mailServerProperties.put("mail.smtp.port", "587");
				mailServerProperties.put("mail.smtp.auth", "true");
				mailServerProperties.put("mail.smtp.starttls.enable", "true");
				System.out.println("Mail Server Properties have been setup successfully..");
		 
				// Step2
				System.out.println("\n\n 2nd ===> get Mail Session..");
				getMailSession = Session.getDefaultInstance(mailServerProperties, null);
				generateMailMessage = new MimeMessage(getMailSession);
				generateMailMessage.addRecipient(Message.RecipientType.TO, new InternetAddress("rami.abidi@esprit.tn"));
				generateMailMessage.addRecipient(Message.RecipientType.CC, new InternetAddress("test2@crunchify.com"));
				generateMailMessage.setSubject("Greetings from Crunchify..");
				String emailBody = "Test email by Crunchify.com JavaMail API example. " + "<br><br> Regards, <br>Crunchify Admin";
				generateMailMessage.setContent(emailBody, "text/html");
				System.out.println("Mail Session has been created successfully..");
		 
				// Step3
				System.out.println("\n\n 3rd ===> Get Session and Send mail");
				Transport transport = getMailSession.getTransport("smtp");
		 
				// Enter your correct gmail UserID and Password
				// if you have 2FA enabled then provide App Specific Password
				transport.connect("smtp.gmail.com", "rami.abidi@esprit.tn", "09970648pirate");
				transport.sendMessage(generateMailMessage, generateMailMessage.getAllRecipients());
				transport.close();
				return "/refcin?faces-redirect=true";
	}
	 public ArrayList<ReferingEmployees> doGetAllR() throws JsonParseException, JsonMappingException, IOException {
		 System.out.println(userejb.getAllR());	
		 return userejb.getAllR();
			
		}
	 
		public String modifyR(int Id) {
			refemp = new ReferingEmployees(); 
			refemp = userejb.getByIdRef(Id);
			refemp.setEmail(emaill);     
	      //cond.setStatus(statu);
			refemp.setLastName(lastNam);
			refemp.setFirstName(firstNam);
			refemp.setPassword(password);
			refemp.setScore(score);
			//refemp.setScore(score.toString());
		
		//userejb.EditById(Id,condidate);
			userejb.updateR(refemp, Id);
			System.out.println(condedit);
	       // cond = cnd.getById(Id);
			return "/refcin?faces-redirect=true";
		}
		
		
		public String AffichRef(int Id) {
			refemp = new ReferingEmployees(); 
		//	System.out.println("//////////////////////"+CndId);
			refemp = userejb.getByIdRef(Id);
			setFirstNam(refemp.getFirstName());
			setLastNam(refemp.getLastName());
			setEmaill(refemp.getEmail());
			setCin(refemp.getCin());
			setScore(refemp.getScore());
			System.out.println(refemp.getScore());
			return "/editref.jsf?faces-redirect=true";
		}
		
		public String doDeleteR(int id) {
	        userejb.deleteR(id);
	        return "refcin.jsf?faces-redirect=true";
	    }
		
		 public String doGetRef(){
				
				Id=Integer.parseInt(Txtcin);
				System.out.println("//////////////////////"+CndId);
				refemp = userejb.getByIdRef(Id);
				
				return "/listref.jsf?faces-redirect=true";	
			}
		 
		 
		 public String doGetR() throws JsonParseException, JsonMappingException, IOException {	
				//System.out.println("sdffdffffffffffffffffffffffffffffffffffffffffffffffff"+liste2.size());
				refemps = doGetAllR();
				System.out.println("********************************************************************************");
				System.out.println(Txtname);
				System.out.println(Txtlname);
				listeR = new ArrayList<ReferingEmployees>();
				for (ReferingEmployees c : refemps) {
					if ((c.getFirstName().toString().equals(Txtname))||(c.getLastName().toString().equals(Txtlname))){
						ReferingEmployees c1 = c;
					    System.out.println("afficheeeee wa7da" + c1);
					    listeR.add(c1); }
				}
				return "/listrefspeci.jsf?faces-redirect=true";
			}
		 
		 public String IndexGetAllR() {
				return "/listref.jsf?faces-redirect=true";
			}
		 public String Indexchartcondit() {
				return "/chartstatr.jsf?faces-redirect=true";
			}
		 public String Indexchartref1() {
				return "/chartstat.jsf?faces-redirect=true";
			}
		 public String Indexchartref2() {
				return "/chart.jsf?faces-redirect=true";
			}
		 
	///////////////////////////////////// condidate //////////////////////////////////////////////
	public String add()throws AddressException, MessagingException
	{
		userejb.AddUser(condidate);
		condidate= new Condidate();
		// Step1
		System.out.println("\n 1st ===> setup Mail Server Properties..");
		mailServerProperties = System.getProperties();
		mailServerProperties.put("mail.smtp.port", "587");
		mailServerProperties.put("mail.smtp.auth", "true");
		mailServerProperties.put("mail.smtp.starttls.enable", "true");
		System.out.println("Mail Server Properties have been setup successfully..");
 
		// Step2
		System.out.println("\n\n 2nd ===> get Mail Session..");
		getMailSession = Session.getDefaultInstance(mailServerProperties, null);
		generateMailMessage = new MimeMessage(getMailSession);
		generateMailMessage.addRecipient(Message.RecipientType.TO, new InternetAddress("rami.abidi@esprit.tn"));
		generateMailMessage.addRecipient(Message.RecipientType.CC, new InternetAddress("test2@crunchify.com"));
		generateMailMessage.setSubject("Greetings from Crunchify..");
		String emailBody = "Test email by Crunchify.com JavaMail API example. " + "<br><br> Regards, <br>Crunchify Admin";
		generateMailMessage.setContent(emailBody, "text/html");
		System.out.println("Mail Session has been created successfully..");
 
		// Step3
		System.out.println("\n\n 3rd ===> Get Session and Send mail");
		Transport transport = getMailSession.getTransport("smtp");
 
		// Enter your correct gmail UserID and Password
		// if you have 2FA enabled then provide App Specific Password
		transport.connect("smtp.gmail.com", "rami.abidi@esprit.tn", "09970648pirate");
		transport.sendMessage(generateMailMessage, generateMailMessage.getAllRecipients());
		transport.close();
		return "/listcondidat.jsf?faces-redirect=true";
		
	}
	
	//public Response modify(){
		//return userejb.update(condidate, 11);
	//}
	
	public String modify(int Id) {
		condedit = new Condidate(); 
		condedit = userejb.getById(Id);
		condedit.setEmail(emaill);     
      //cond.setStatus(statu);
		condedit.setLastName(lastNam);
		condedit.setFirstName(firstNam);
		condedit.setPassword(password);
		condedit.setStatus(null);
	//userejb.EditById(Id,condidate);
		userejb.update(condedit, Id);
		System.out.println(condedit);
       // cond = cnd.getById(Id);
		return "/condidatecin?faces-redirect=true";
	}

	public String AffichCondida(int Id) {
		condd = new Condidate(); 
	//	System.out.println("//////////////////////"+CndId);
		condd = userejb.getById(Id);
		setFirstNam(condd.getFirstName());
		setLastNam(condd.getLastName());
		setEmaill(condd.getEmail());
		setCin(condd.getCin());
		return "/edituser.jsf?faces-redirect=true";
	}
	
	
	
	
	
	
	 public String doDelete(int id) {
	        userejb.delete(id);
	        return "condidatecin.jsf?faces-redirect=true";
	    }
	 public List<Condidate> doGetAll() throws JsonParseException, JsonMappingException, IOException {
			return userejb.getAll();
		}
	
	 public String doGetCondidat(){
			
			Id=Integer.parseInt(Txtcin);
			System.out.println("//////////////////////"+CndId);
			condidate = userejb.getById(Id);
			
			return "/listcondidat.jsf?faces-redirect=true";	
		}
		///////////////////////////////////////stat/////////////////////////////
		public long statCondidatA() throws JsonParseException, JsonMappingException, IOException{
			//userejb.getAll().stream().filter((u)->getStatu()==statu.Accepted)
			//return	 userejb.getAll().stream().filter((cdid)->getStatus()==States.Accepted).count();
			return	 userejb.getAll().stream().filter((condd)->getStatus()==States.Accepted).count();
		}
		
		public long statCondidatR() throws JsonParseException, JsonMappingException, IOException{
			//userejb.getAll().stream().filter((u)->getStatu()==statu.Accepted)
			return	 userejb.getAll().stream().filter((condd)->getStatus()==States.Refused).count();
		}
		
		
		
		private static final long serialVersionUID = 1L;
		private LineChartModel animatedModel1;
		public LineChartModel getAnimatedModel1() {
			return animatedModel1;
		}
		public void setAnimatedModel1(LineChartModel animatedModel1) {
			this.animatedModel1 = animatedModel1;
		}
		

		public static long getSerialversionuid() {
			return serialVersionUID;
		}
		public void createAnimatedModels() throws JsonParseException, JsonMappingException, IOException {
	        animatedModel1 = initLinearModel();
	        animatedModel1.setTitle("Line Chart");
	        animatedModel1.setAnimate(true);
	        animatedModel1.setLegendPosition("se");
	        Axis yAxis = animatedModel1.getAxis(AxisType.Y);
	        yAxis.setMin(0);
	        yAxis.setMax(10);
	         
			
	    }
	     

	    public BarChartModel initBarModel() throws JsonParseException, JsonMappingException, IOException {
	        BarChartModel model = new BarChartModel();
	 
	        ChartSeries boys = new ChartSeries();
	        boys.setLabel("Customers");
	      
	        boys.set("success", statCondidatA());
	        boys.set("faile", statCondidatR());
	  
	        
	 
	        model.addSeries(boys);
	      
	         
	        return model;
	    }
	    
	    
	    public LineChartModel initLinearModel() throws JsonParseException, JsonMappingException, IOException {
	        LineChartModel model = new LineChartModel();
	 
	        LineChartSeries series1 = new LineChartSeries();
	        series1.setLabel("Success");
	 
	        series1.set(statCondidatA(),statCondidatA());
	      /*  series1.set(2, 1);
	        series1.set(3, 3);
	        series1.set(4, 6);
	        series1.set(5, 8);*/
	 
	        LineChartSeries series2 = new LineChartSeries();
	        series2.setLabel("failed ");
	 
	        series2.set(statCondidatR(), statCondidatR());
	       /* series2.set(2, 3);
	        series2.set(3, 2);
	        series2.set(4, 7);
	        series2.set(5, 9);*/
	 
	        model.addSeries(series1);
	        model.addSeries(series2);
	         
	        return model;
	    }
	    

		//////////////////////////////////////////stat///////////////////////////////////////////
		public String doGet() throws JsonParseException, JsonMappingException, IOException {	
			//System.out.println("sdffdffffffffffffffffffffffffffffffffffffffffffffffff"+liste2.size());
			refemps = doGetAllR();
			System.out.println("********************************************************************************");
			System.out.println(Txtname);
			System.out.println(Txtlname);
			liste2 = new ArrayList<Condidate>();
			for (Condidate c : mstCode) {
				if ((c.getFirstName().toString().equals(Txtname))||(c.getLastName().toString().equals(Txtlname))){
					Condidate c1 = c;
				    System.out.println("afficheeeee wa7da" + c1);
					liste2.add(c1); }
			}
			return "/listcondidatspeci.jsf?faces-redirect=true";
		}

		
		public String IndexGetAll() {
			return "/listcondidat.jsf?faces-redirect=true";
		}
		////////////////////////////////////Company////////////////////////////////
	/*
	 * public User getUserByEmailAndPassword(String email, String password) {
		TypedQuery<User> query = em.createQuery("select u from User u "+
				"where u.email=:email and "+
						"u.password=:password " ,User.class);
				query.setParameter("email",email);
				query.setParameter("password", password);
				User user = null;
				try{
					user=query.getSingleResult();
				}catch(NoResultException e){
					Logger.getGlobal().info("Aucun employe trouve avec amail: " + email);
					
				}
				return user;
	}
		
		public HumanRessources getuserbyEmailandPassword(String email,String password) throws JsonParseException, JsonMappingException, IOException
		{
			//userejb.getAllh().stream().filter((u)->
		List<HumanRessources> user = userejb.getAllhu().stream().filter(x -> x.getMailCompany() == email && x->x.).collect(Collectors.toList());
		
			
		}*/
		
		public String log(String login,String password) throws JsonParseException, JsonMappingException, IOException{
			if(userejb.getAllh().getNameCompany()=="password" && userejb.getAllh().getMailCompany()=="username"){
				System.out.println(userejb.getAllh().getNameCompany());
			 return "/listcondidat.jsf?faces-redirect=true";
			}
			return "/condidatecin.jsf?faces-redirect=true";
		}
		public String doLogin() {
			try {for(Condidate user : userejb.getAll())
			{
			
				if(user.getFirstName().toString().equals(firstNam))
				{
					if(user.getPassword().toString().equals(password))
					{
						System.out.println("sa7itek");
						  userejb.logIn(null);
						  return "index.jsf?faces-redirect=true";
					}
				}				
			}}
			catch(Exception e){
		System.out.println("ouh 3lya");	
		return "index.jsf?faces-redirect=true";}
		//	return "./";
			 return "index.jsf?faces-redirect=true";
		
		}
////////////

		public void setCondd(Condidate condd) {
			this.condd = condd;
		}

		public void setCond(Condidate cond) {
			this.cond = cond;
		}

	

	}

